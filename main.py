import json
from py_docker import SummaryInfo

if __name__ == '__main__':
    data: dict = SummaryInfo.generic_status()
    print(json.dumps(data, indent=3))
